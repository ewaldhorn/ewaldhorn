### Hi there 👋
This is my GitLab profile, true story!

## What's here
Just some things I've worked on.  I am always exploring technologies, trying new things and learning in the process.  You'll find some code I've worked on here, but please be careful running it on your machine - it is experimental, after all!

## Who am I
Who cares? I'm just some random chap on the internet doing random things in random technologies.  Don't believe all the stories you hear. I probably don't know what I'm doing, certainly don't know what I'm talking about and most definitely should not be allowed to write code that's seen by humans or compilers.

## What I do
Stuff. Lots and lots of stuff.  Usually, I'll be annyoing the Rust or Kotlin compilers by either building esoteric Android apps or truly weird and whacky desktop apps using emerging technologies like Jetpack Compose or Tauri.  If it's something that needs doing, I'll get busy doing it.  Peeps looking to throw money at me for solving their problems can visit my [most awesome website](https://www.nofuss.co.za/) for more information.

## More code
Err, if you want to see more code, I also use [Github](https://github.com/ewaldhorn) as a code repository. I find both services offer different value propositions and it's handy to have some knowledge of both Gitlab and Github.

## What's next
Who knows?!  I'm hoping more Go, WebAssembly and Zig though.
